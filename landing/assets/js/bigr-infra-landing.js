$(document).ready(populate_components()); 

function populate_components() {
    axios.get('data/site-map.json')
    .then(function (response) {
      console.log(response['data']);
      // site_map = JSON.parse(response['data']);
      generate_links(response['data']);
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('data/partner-map.json')
    .then(function (response) {
      console.log(response['data']);
      // site_map = JSON.parse(response['data']);
      generate_logo_grid(response['data']);
    })
    .catch(function (error) {
      console.log(error);
    });
}

function generate_links(site_map) {
    var app_container = $("#apps");
    for (var key in site_map) {
        var content = `
          <div class="card">
            <img class="card-img-top" src="${site_map[key].image}" alt="${key}">
            <div class="card-body d-flex flex-column">
              <h4 class="card-title">${site_map[key].title}</h2>
              <div class="card-text">${site_map[key].desc}</div>
              <a href="${window.location.protocol}//${window.location.hostname}:${site_map[key].port}/${site_map[key].path}" target="_blank" class="mt-auto btn-lg btn btn-primary stretched-link">Have a look &raquo;</a>
            </div>
          </div>`;
        app_container.append(content);
    }
}

function generate_logo_grid(partner_map) {
  var partner_container = $("#partners");
  for (var p in partner_map) {
    var data = partner_map[p];
    var content = `
      <div class="col-sm">
        <a href="${data.website}" target="_blank">
          <img src="${data.image}" alt="${data.tooltip}" width="150px">
        </a>
      </div>`;
    partner_container.append(content);
  }
}