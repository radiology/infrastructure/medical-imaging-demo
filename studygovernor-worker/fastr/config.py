# [bool] Flag to enable/disable debugging
debug = False

# [str] Directory containing the fastr examples
examplesdir = "/usr/local/lib/python3.7/site-packages/fastr/examples"

# [str] The default execution plugin to use
execution_plugin = "ProcessPoolExecution"
process_pool_worker_number = 2

# [str] Execution script location
executionscript = "/usr/local/lib/python3.7/site-packages/fastr/execution/executionscript.py"

# [str] Redis url e.g. redis://localhost:6379
filesynchelper_url = ""

# [str] The level of cleanup required, options: all, no_cleanup, non_failed
job_cleanup_level = "no_cleanup"

# [bool] Indicate if default logging settings should log to files or not
# log_to_file = False

# [str] Directory where the fastr logs will be placed
logdir = "/root/.fastr/logs"

# [dict] Python logger config
logging_config = {}

# [int] The log level to use (as int), INFO is 20, WARNING is 30, etc
loglevel = 20

# [str] Type of logging to use
logtype = "default"

# [dict] A dictionary containing all mount points in the VFS system
mounts = {
  "tmp": "/tmp",
  "example_data": "/usr/local/lib/python3.7/site-packages/fastr/examples/data",
  "home": "/root/"
}

# [list] Directories to scan for networks
networks_path = [
  "/usr/local/lib/python3.7/site-packages/fastr/resources/networks"
]

# [list] Directories to scan for plugins
plugins_path = [
  "/usr/local/lib/python3.7/site-packages/fastr/resources/plugins"
]

# [list] A list indicating the order of the preferred types to use. First item is most preferred.
preferred_types = []

# [list] A list of modules in the environmnet modules that are protected against unloading
protected_modules = []

# [list] The reporting plugins to use, is a list of all plugins to be activated
reporting_plugins = [
  "SimpleReport"
]

# [str] Directory containing the fastr system resources
resourcesdir = "/usr/local/lib/python3.7/site-packages/fastr/resources"

# [str] Directory containing the fastr data schemas
schemadir = "/usr/local/lib/python3.7/site-packages/fastr/resources/schemas"

# [int] The number of source jobs allowed to run concurrently
source_job_limit = 0

# [str] Fastr installation directory
systemdir = "/usr/local/lib/python3.7/site-packages/fastr"

# [list] Directories to scan for tools
tools_path = [
  "/usr/local/lib/python3.7/site-packages/fastr/resources/tools",
  "/home/fastr/demo-tools"
]

# [list] Directories to scan for datatypes
types_path = [
  "/usr/local/lib/python3.7/site-packages/fastr/resources/datatypes", 
  "/home/fastr/demo-types/"
]

# [str] Fastr user configuration directory
userdir = "/root/.fastr"

# [bool] Warning users on import if this is not a production version of fastr
warn_develop = True

# [str] The hostname to expose the web app for
web_hostname = "localhost"

# [int] The port to expose the web app on
web_port = 5000

# [str] The secret key to use for the flask web app
web_secret_key = "VERYSECRETKEY!"

reporting_plugins += ['PimReporter']
pim_host = 'https://pim-production.scaleout-emc.surf-hosted.nl'