import itk
import numpy as np 
import csv

def read_image(image_path):
    InputPixelType = itk.SS 
    Dimension = 3 
    InputImageType = itk.Image[InputPixelType, Dimension] 
    reader = itk.ImageFileReader[InputImageType].New()                                                                  
    reader.SetFileName(image_path)
    reader.Update()
    return reader.GetOutput()


def get_stat(image):
    spacing = image.GetSpacing()
    size = image.GetLargestPossibleRegion().GetSize()
    #image = itk.GetArrayFromImage(itk_image).astype(float)
    assert len(size) == 3, "The image has unsupported number of dimensions. Only 3D images are allowed"
    statFilter = itk.StatisticsImageFilter[itk.Image[itk.SS, 3]].New()                                                        
    statFilter.SetInput(image)                                                                                          
    statFilter.Update()                                                                                                 
    sum = statFilter.GetSum()
    return size, spacing, sum * spacing[0] * spacing[1] *spacing[2]


def write_result(size, spacing, volume, path):
    with open(path, mode='w') as stat_result:
        stat_writer = csv.writer(stat_result, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        stat_writer.writerow(['Pixel Spacing', np.array(spacing)])
        stat_writer.writerow(['Image Size', np.array(size)])
        stat_writer.writerow(['Volume', volume])

if __name__ == "__main__":
    import sys
    import argparse
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input image file name. should be '
                                        'nifti (.nii.gz) and can only be a 3D image.', required=True, type=str)
    parser.add_argument('-o', '--output', help='output. should be a filename with .csv extension. If it does not exist, result.scv')

    args = parser.parse_args()
    image = read_image(args.input)
    size, spacing, volume = get_stat(image)
    output_file = args.output

    if output_file is None:
        output_file = os.path.join(os.path.dirname(args.input),'result.csv')
    else:
         if not output_file.endswith('.csv'):
            output_file += '.csv'
    write_result(size, spacing, volume, output_file)
     
