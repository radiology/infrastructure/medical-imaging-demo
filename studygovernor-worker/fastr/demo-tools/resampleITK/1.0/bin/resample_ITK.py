#!/usr/bin/env python
import itk
import sys

if len(sys.argv) != 4:
    print("Usage: " + sys.argv[0] + " <inputImage> <outputImage> <scale>")
    sys.exit(1)

inputImage = sys.argv [1]
outputImage = sys.argv[2]
scale = float(sys.argv[3])

inputImage = itk.imread(inputImage)
PixelType = itk.SS
ScalarType = itk.D
Dimension = inputImage.GetImageDimension()
ImageType = itk.Image[PixelType, Dimension]

#get input spacing
size = inputImage.GetLargestPossibleRegion().GetSize()
spacing = inputImage.GetSpacing()
print("Input Spacing: {}".format(spacing))

#set ouotput spacing according to scale
outputSpacing = inputImage.GetSpacing()
outputSpacing[0] = inputImage.GetSpacing()[0] / scale
outputSpacing[1] = inputImage.GetSpacing()[1] / scale
outputSpacing[2] = inputImage.GetSpacing()[2] / 1
print("Output Spacing: {}".format(outputSpacing))

#set new size according to scale
newSize = inputImage.GetLargestPossibleRegion().GetSize() 
newSize[0] = int(newSize[0] * scale)
newSize[1] = int(newSize[1] * scale)
newSize[2] = int(newSize[2] * 1)
scaleTransform = itk.ScaleTransform[ScalarType, Dimension].New()

#set parameters for resampler
parameters = scaleTransform.GetParameters()
parameters[0] = scale
parameters[1] = scale
parameters[2] = 1

scaleTransform.SetParameters(parameters)
interpolatorType = itk.NearestNeighborInterpolateImageFunction[ImageType, ScalarType]
interpolator = interpolatorType.New()

resamplerType = itk.ResampleImageFilter[ImageType, ImageType]
resampleFilter = resamplerType.New()
resampleFilter.SetInput(inputImage)
resampleFilter.SetOutputParametersFromImage(inputImage)
resampleFilter.SetTransform(scaleTransform)
resampleFilter.SetInterpolator(interpolator)
resampleFilter.SetSize(newSize)
resampleFilter.SetOutputSpacing(outputSpacing)
resampleFilter.SetOutputDirection(inputImage.GetDirection())

#write resampled image
WriterType = itk.ImageFileWriter[ImageType]
writer = WriterType.New()
writer.SetFileName(outputImage)
writer.SetInput(resampleFilter.GetOutput())

writer.Update()
