# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fastr


def create_network():
    network = fastr.create_network(id="hd_bet")

    # Source images
    source_image = network.create_source('DicomImageFile', id='input')
   
    # Dicom conversions

    image_dicom_to_nifti = network.create_node('image_analysis/tools/dcm2nii/DicomToNifti:0.1', id='image_dicom_to_nifti', tool_version ='0.1')
    image_dicom_to_nifti.inputs['dicom_image'] = source_image.output
    
    # use hd-bet for Automated brain extraction
    HDBET_brain_extraction = network.create_node('image_analysis/tools/hd_bet/HdBet:1.0', id='HDBET_brain_extraction', tool_version ='1.0')
    HDBET_brain_extraction.inputs['input'] = image_dicom_to_nifti.outputs['image']

    # Save extracted brain images with a sink
    brainExtract = network.create_sink('NiftiImageFileCompressed', id='brainExtract')
    brainExtract.inputs['input'] = HDBET_brain_extraction.outputs['image']

    
    return network

