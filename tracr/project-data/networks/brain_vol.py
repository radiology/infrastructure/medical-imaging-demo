import fastr

def create_network():
  ''' complete this function to create a fastr network for brain volume extraction '''

  # create source node

  # Create node for volume calculation

  # Save result in a sink node


if __name__ == "__main__":
    network = create_network()

    
    sources = {"brain_mask": "/scans/6/resources/NIFTI/files/t1w_brainmask_fslbet{ext}"}

    # Fill in the appropriate sink path ( eg. "your sink name": "/scans/6/resources/NIFTI/files/volume{ext}") 
    sinks = {}
    network.execute(sources, sinks, tmpdir='/root/test-run')