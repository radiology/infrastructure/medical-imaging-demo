Using python from /usr/local/bin/python
PYTHONPATH=
[INFO] basepluginmanager:0078 >> Could not load plugin file /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/ioplugins/s3filesystem.py
FastrImportError from /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/ioplugins/s3filesystem.py line 59: Could not import the required boto3/botocore modules for this plugin
[INFO] basepluginmanager:0078 >> Could not load plugin file /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/reportingplugins/elasticsearchreporter.py
FastrOptionalModuleNotAvailableError from /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/reportingplugins/elasticsearchreporter.py line 46: Could not import the required elasticsearch for this plugin
[INFO] networkrun:0517 >> ####################################
[INFO] networkrun:0518 >> #     network execution STARTED    #
[INFO] networkrun:0519 >> ####################################
[INFO] networkrun:0544 >> Running network via /usr/local/lib/python3.7/site-packages/fastr/api/__init__.py (last modified Wed Nov 20 08:51:49 2019)
[INFO] networkrun:0545 >> FASTR loaded from /usr/local/lib/python3.7/site-packages/fastr
[INFO] networkrun:0561 >> Network run tmpdir: /tmp/fastr_hd_bet_1
[INFO] networkchunker:0146 >> Adding downsampled_nii to candidates (blocking False)
[INFO] networkchunker:0146 >> Adding HDBET_brain_extraction to candidates (blocking False)
[INFO] networkchunker:0146 >> Adding brainExtract to candidates (blocking False)
[INFO] networkchunker:0146 >> Adding brainMask to candidates (blocking False)
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/input sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO] basepluginmanager:0078 >> Could not load plugin file /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/targetplugins/singularitytarget.py
Could not find executable "singularity" on PATH: ['/usr/local/bin', '/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/usr/bin', '/sbin', '/bin']
[INFO] basepluginmanager:0078 >> Could not load plugin file /usr/local/lib/python3.7/site-packages/fastr/resources/plugins/executionplugins/slurmexecution.py
Could not find executable "sbatch" on PATH: ['/usr/local/bin', '/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/usr/bin', '/sbin', '/bin']
[INFO] networkrun:0620 >> Waiting for execution to finish...
[INFO] networkrun:0747 >> Finished job hd_bet___input___ANONYMIZ001_BRAIN with status JobState.finished
[INFO] networkrun:0631 >> Chunk execution finished!
[INFO]   noderun:0470 >> Generating jobs for node "image_dicom_to_nifti" with dimensions: [input: 1]
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/image_dicom_to_nifti sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO]   noderun:0470 >> Generating jobs for node "downsampled_nii" with dimensions: [input: 1]
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/downsampled_nii sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO]   noderun:0470 >> Generating jobs for node "HDBET_brain_extraction" with dimensions: [input: 1]
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/HDBET_brain_extraction sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/brainExtract sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO]   noderun:0576 >> Creating job for node fastr:///networks/hd_bet/0.0/runs/hd_bet_2019-11-20T10-09-41/nodelist/brainMask sample id <SampleId ('ANONYMIZ001_BRAIN',)>, index <SampleIndex (0)>
[INFO] networkrun:0620 >> Waiting for execution to finish...
[INFO] networkrun:0747 >> Finished job hd_bet___image_dicom_to_nifti___ANONYMIZ001_BRAIN with status JobState.finished
[INFO] networkrun:0747 >> Finished job hd_bet___downsampled_nii___ANONYMIZ001_BRAIN with status JobState.finished
[INFO] networkrun:0747 >> Finished job hd_bet___HDBET_brain_extraction___ANONYMIZ001_BRAIN with status JobState.finished
[INFO] networkrun:0747 >> Finished job hd_bet___brainExtract___ANONYMIZ001_BRAIN___0 with status JobState.finished
[INFO] networkrun:0747 >> Finished job hd_bet___brainMask___ANONYMIZ001_BRAIN___0 with status JobState.finished
[INFO] networkrun:0631 >> Chunk execution finished!
[INFO] executionplugin:0490 >> Callback processing thread for ProcessPoolExecution ended!
[INFO] networkrun:0633 >> ####################################
[INFO] networkrun:0634 >> #    network execution FINISHED    #
[INFO] networkrun:0635 >> ####################################
[INFO] simplereport:0026 >> ===== RESULTS =====
[INFO] simplereport:0035 >> brainExtract: 1 success / 0 failed
[INFO] simplereport:0035 >> brainMask: 1 success / 0 failed
[INFO] simplereport:0036 >> ===================
