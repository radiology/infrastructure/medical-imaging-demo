# HEALTH-RI bigr infrastructure Demo 

## General Info

Bigr infrastructure demo at Health-RI conference 2019, januari 17

*Far from perfect*

---

## Getting started

### Launch dockers
1. docker-compose config (Check if compose file is valid)
2. docker-compose pull
3. docker-compose build (Build images)
5. docker-compose down && docker-compose up -d && docker-compose logs -f

*In case of updated pull/clone all repos and rebuild the docker images*

### Going to the langing page
Use your browser to open the landing page. Depending on the exact setup this should be available on localhost:8000 or when using dockermachine use the IP-Address (ie 192.168.99.100:8000) of said machine.

### Configure the hostname (OPTIONAL)

#### Linux
1. Add `127.0.0.1  demo.healthri.local` to the /etc/hosts file

#### Mac
1. ???

---

## TODO

### Create syncotron workflow
    Current state is just the poster workflow ( not actually working)
### Create tasks
    1. Manual QA
    2. Manual QC
### Create templates
    1. Manual QA
    2. Manual QC

---

## In case of issues

FIX ISSUES ;)
