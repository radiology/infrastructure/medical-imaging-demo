import xnat

# Set project/host
host = 'http://192.168.99.102:8080'
project = 'sandbox'

# Connect
xnat_host = xnat.connect(host)
xnat_project = xnat_host.projects[project]
xnat_subject = xnat_project.subjects['ANONYMIZ']
xnat_experiment = xnat_project.experiments['ANONYMIZ']

# Upload T1 weighted
xnat_scan = xnat_experiment.scans['T1W']
xnat_resource = xnat_host.classes.ResourceCatalog(parent=xnat_scan, label='NIFTI')
xnat_resource.upload('./sample_data/t1w.nii.gz', 't1w.nii.gz')
xnat_resource.upload('./sample_data/t1w_bet_mask.nii.gz', 't1w_brain_mask.nii.gz')
xnat_resource.upload('./sample_data/flair_t1wspace.nii.gz', 'flair_t1wspace.nii.gz')
xnat_resource.upload('./sample_data/pd_t1wspace.nii.gz', 'pd_t1wspace.nii.gz')

# Upload PD weighted
xnat_scan = xnat_experiment.scans['PDW']
xnat_resource = xnat_host.classes.ResourceCatalog(parent=xnat_scan, label='NIFTI')
xnat_resource.upload('./sample_data/pd.nii.gz', 'pdw.nii.gz')

# Upload flair
xnat_scan = xnat_experiment.scans['FLAIR']
xnat_resource = xnat_host.classes.ResourceCatalog(parent=xnat_scan, label='NIFTI')
xnat_resource.upload('./sample_data/flair.nii.gz', 'flair.nii.gz')
xnat_host.disconnect()