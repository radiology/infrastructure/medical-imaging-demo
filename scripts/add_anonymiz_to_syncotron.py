import requests
import json

syncotron_host = "http://192.168.99.102:5000"

def create_subject():
    xnat_subject_id = "ANONYMIZ"
    subject_data = {
        "date_of_birth": "2000-01-01",
        "label": xnat_subject_id,
    }
    response = requests.post(syncotron_host + '/data/subjects', json=subject_data)
    if response.status_code > 400:
        print('Error creating subject')
        exit(1)
    print(response)
    synco_subject = json.loads(response.text)
    external_subject_data = {
        "subject": synco_subject["uri"],
        "external_system": "/data/external_systems/XNAT",
        "external_id": xnat_subject_id,
    }
    response = requests.post(syncotron_host + "/data/external_subject_links", json=external_subject_data)
    print(response)
    return synco_subject

def create_experiment():
    xnat_experiment_id = "ANONYMIZ"
    experiment_data = {
        "scandate": "2000-01-01T19:57:25",
        "subject": "ANONYMIZ",
        "label": xnat_experiment_id
    }
    response = requests.post(syncotron_host + '/data/experiments', json=experiment_data)
    if response.status_code > 400:
        print('Error creating subject')
    print(response)
    synco_sexperiment = json.loads(response.text)
    external_experiment_data = {
        "external_id": xnat_experiment_id,
        "experiment": synco_sexperiment["uri"],
        "external_system": "/data/external_systems/XNAT"
    }
    response = requests.post(syncotron_host + "/data/external_experiment_links", json=external_experiment_data)
    print(response)

if __name__ == '__main__':
    #main()   
    synco_subject = create_subject()
    synco_experiment = create_experiment()
