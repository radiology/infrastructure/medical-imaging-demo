# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Imaging Research Refinery Overview'
copyright = '2019, Hakim Achterberg, Adriaan Versteeg,\
               Marcel Koek, Mahlet Birhanu'
author = 'Hakim Achterberg, Marcel Koek, \
          Adriaan Versteeg,  Mahlet Birhanu.'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Options for latex output -------------------------------------------------

# Additional stuff for the LaTeX preamble.

latex_elements = {
# The paper size ('letter' or 'a4').
'papersize': 'letter',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# remove blank pages (between the title page and the TOC, etc.)
'classoptions': ',openany,oneside',
'babel' : '\\usepackage[english]{babel}',

# necessary for unicode charactacters in pdf output
'inputenc': '',
'utf8extra': '',
'preamble': r'''
  \usepackage{hyperref}
  \setcounter{tocdepth}{3}
'''
}

# If false, no module index is generated.
latex_use_modindex = True

# A comma-separated list of custom stylesheets. Example:
pdf_stylesheets = ['sphinx','kerning','a4']

