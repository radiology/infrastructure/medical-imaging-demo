.. Infrastructure Tutorial documentation master file, created by
   sphinx-quickstart on Mon Nov  4 09:54:23 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Imaging Research Refinery Tutorial!
===============================================


Requirements
============

Using tutorial machine in the cloud
************************************
    * Remote Desktop Client
    * SSH client (mobaXterm)

Using you own device
*************************************
    * Remote Desktop Client
    * SSH client (windows: mobaXterm)
    * docker
    * docker-compose
    * docker-machine if running Mac OS/Windows
    * git

.. toctree::
    :maxdepth: 2
    :caption: Contents:
   
    introduction
    deployment_and_configuration
    XNAT
    anonymization
    tracking_experiments
    managing_tasks
    manual_anotation_inspection
    automated_processing
    assignments
    known_issues


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
