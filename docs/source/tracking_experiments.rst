===============================================
Tracking Experiments
===============================================

Any scan session in XNAT is referred to as an experiment. An experiment/
scan session of a subject/patient can pass through many states until the 
final goal is achieved that is specified by the project. Many population 
studies and researches usually involve tons of experiments and subjects. 
As the number of experiments increases, it becomes very complicated to 
keep track and count of states of experiments. A Study Governor becomes 
very handy in automatically keeping track of which state each experiment 
is.

Study governor is a tool developed in our infrastructure team. It is a 
service that keeps track of the state (and previous states) of each 
experiment stored in XNAT. It makes sure that each experiment will go 
through a certain workflow. Typically, an experiment is first in an 
"untracked state" when it is first created. Then it moves to "Quality 
Assurance" (Manual QA) state. The next states can be seen in the 
figure below.  Checks are built into the study governor to make sure only 
specific transitions are allowed between states.

Checkout the `Study Governor Documentation <https://idsp-study-governor.readthedocs.io/en/latest/>`_ for more information. 

.. image:: _static/healthri-demo.png

We will now interface the study manager (study governor) tool in the 
following steps.


#. Go to the study governor web page from the landing page and log in 
   using the credentials: ``demo:demo``.

#. The next step will be to add subject and experiments to the study 
   governor. Again we will be using the REST API of the study governor 
   service. (You can find the documentation/swagger interface from the 
   main page of the study governer service).

   To do so, in the jupyter notebook, under the "ADDING SUBJECT/EXPERIMENT 
   TO SYNCOTRON" header, two functions are defined to create subjects and 
   experiments. Run definition and the function calling cell (use ``Shift 
   + ENTER``).

   If you look closer to the code you will see that for creating a subject 
   and experiment we actually perform two POST functions to create the 
   subject/experiment including date of birth and scandate. For each of 
   those we also create a external_subject_data and external_experiment_data. 
   These are used to link data stored on other services, like XNAT. 

   .. code-block :: text

      Execute blocks:
      # 5.1 Define function for adding subject and experiments to the study governor
      # 5.2 Add subject/experiment to study governor


#. Return to the study governor page. You should be able to see a subject and 
   an experiment added. Additionally, you can see in which state the experiment 
   is and transition overview/guide of how experiments transition from one 
   state to another.
