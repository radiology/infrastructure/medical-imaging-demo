===============================================
Known issues
===============================================

==============  =================== ========== =======================================================
Service         Error Message                   Error Code Solution/Cause
==============  =================== ========== =======================================================
MYSQL           MySQL not loading    1114       Disk is full
Jupyter         Authorization Error             Reload Jupyter from LoadingPage
XNAT            Session Expired                 Disk space issue
XNAT            Not receving DICOMS             Restart DICOM SCP. XNAT-> Administrator 
                                                -> Site Administration -> DICOM SCP Receivers -> Enable
CPT             Error Socket                    sudo usermod -aG docker <your-user>      
                Not Connected
docker-compose  Permission denied:
                './.env'                        install docker-compose from official instructions  
==============  =================== ========== =======================================================
