===============================================
XNAT
===============================================

XNAT is an open source imaging informatics platform developed by the 
Neuroinformatics Research Group at Washington University. XNAT was 
originally developed at Washington University in the Buckner Lab, which 
is now located at Harvard University. It facilitates common management, 
productivity, and quality assurance tasks for imaging and associated data. 
Thanks to its extensibility, XNAT can be used to support a wide range of 
imaging-based projects.

For the infrastructure we developed, we use XNAT as the major tool to 
organize imaging data storage. In XNAT, users can set up projects that 
can contain multiple experiments taken from different subjects. Data is 
organized in XNAT in the following manner.  

*  Any person can create a **project** and upload data, thereby becoming 
   a "Project Owner." This gives you full permissions to create, upload, 
   modify and delete data within that project. However, in someone else's 
   project, you may be given fewer permissions.

*  A **subject** is anyone who participates in a study. XNAT has traditionally 
   focused on human studies. A subject cannot exist outside the context of a 
   project. A subject record is 'owned' by the project which created it. 
   Additionally, subjects can be registered (shared) with other projects.

*  An **experiment** is an event by which data is acquired. This data can be 
   imaging data, or non-imaging data. Similar to a subject, an experiment cannot 
   exist outside the context of a project. It is owned by the project which 
   first added it. It can additionally be shared into other projects.

We will now go through the first component of our infrastructure, i.e . XNAT, 
by following the following steps.

#.  From the **landing page** i.e. (http://YOUR-MACHINE:8000), open the XNAT 
    webpage and login with the credentials: ``admin:admin``. 

#.  Complete the XNAT site setup configuration (It is possible to leave the 
    default values for configuration). Press ``Save all`` to continue to the 
    main XNAT page.
    
#.  After XNAT is setup correctly, we can make create our project. We can do 
    this using the XNAT interface or by using the REST API to program it.

    .. note::

        A RESTful API is an application program interface (API) that uses HTTP 
        requests to GET, PUT, POST and DELETE data. It gives an interface to the 
        background operations of a web application.

    During this tutorial we will be using REST API for most of the tasks. All 
    the code necessary can be found in the jupyter notebook, which you can open 
    from the landing page. 

#.  Execute the code (use ``Shift + ENTER``) in the first cell to create an XNAT 
    project. Return to the XNAT webpage opened in the previous steps and navigate 
    to check if the project ``sandbox`` created is there. For the time being, project 
    sandbox contains no data. We will see how data is added in the next section.
   
   
.. warning::

    It is important to note that, for reproducibility purposes, it is best to create 
    resources like projects via the API, even for small projects. Keep these scripts 
    in a repository for future reference.


