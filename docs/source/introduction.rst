===============================================
Introduction
===============================================

The goal for the tutorial/winter school day is to get an understanding on how to perform large imaging cohort and population studies in a standardized and  reproducible way. We will introduce our own developed infrastructure to facilitates this.

.. image:: _static/general_overview_2.png

This figure shows a complete overview of the infrastructure. During this tutorial we will go through all the steps and explain what each component does and how the tools collaborate to deliver the anticipated goal.