***********
Assignments
***********


--------------------------
FastR automated processing
--------------------------

During this assignment we will wrap a tool for use in the FastR pipeline 
engine and incorporate it into a pipeline. During the workshop we have 
processed a dataset which was visually inspected. Now, we want to calculate 
the biomarker. For this we have written an example application that 
calculates the volume of a mask, in our case the brain mask.

The tool (found in synco_worker/fastr/demo-tools/BrainVol/) has the following interface (please don't try to run this, see the warning below):

.. code-block:: console

    brain_volume --input <input image> --output <csv>

.. warning::

    When you want to run the command above you have to do that from the worker container, you can get into this container via:

    .. code-block:: console

        docker exec -it synco_worker /bin/bash
        cd /home/fastr/demo-tools/BrainVol/bin
        ./brain_volume.py --help

   
#.  Wrap this tool in Fastr. For reference, other Fastr tool definitions can be found in the synco worker folder. We already 
    created the folder structure (.../demo-tools/BrainVol/) for this tool needed so you can just create a new XML file there. 

    .. note::

        Checkout the `Fastr Documentation <https://fastr.readthedocs.io/en/stable/>`_.

        In the documentation is described how `to wrap your tool <https://fastr.readthedocs.io/en/stable/static/user_manual.html#create-your-own-tool>`_

    After writting the tool definition (in XML), you can verify if it works using fastr. Fastr itself is available
    in the synco_worker container. On a new terminal inside your virutal machine, you can enter this container using:

    .. code-block:: console

        docker exec -it synco_worker /bin/bash

    Inside this container, you can try to run fastr verify when in the fastr-tools directory (/home/fastr-tools).

    .. code-block:: console

        fastr verify tool /home/fastr-tools/image_analysis/tools/BrainVol/TOOL-NAME.xml 

#.  Add the newly created tool to the pipeline. Example/reference files are located at the /tracr/project-data/networks folder. 
    Files in this folder are used to create a fastr network to be used in the pipeline by employing existing tools. To do so, complete the code the file (brain_vol.py). 
    
    You should be able to run the network by executing the python file. (We added some magic to load the default data from XNAT).

    .. code-block:: console

        python /project/networks/brain_vol.py

    If everything is correct we should see fastr finish correctly, at the end of the stdout fastr prints a command to start debugging the pipeline.


-----------------------
Creating a ViewR Layout
-----------------------

During this assingment we will create a new "ViewR" to assess the automated processing in the first part of the tutorial. 
For this we need to create a task template and add that to the taskmanager.

The template is created in a JSON file. This JSON should be created in the trace/project-data/templates folder ie create a file called: BET-assessment.
Checkout the `ViewR Documentation <https://viewr.readthedocs.io/en/latest/>`_

We can use a jupyter notebook in the jupyter container. Open the jupyter notebook from the **Landing Page** using the menu open a new notebook (code-snippets). Here you can find some examples on how to upload the new template to the taskmanager. And create tasks using this new template.
You can check the templates in the TaskManager interface. After the tasks are added you can open the ViewR to inspect/assess the data.
