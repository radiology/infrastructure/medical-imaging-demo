===============================================
Manual Annotation & Inspection
===============================================

Once an experiment is staged for manual tasks, the authorized user has to complete the annotation and/or correction tasks so that the experiment can successfully transition to the next state. 
ViewR is a separate tool developed by our infrastructure team and is used to perform annotation and correction tasks on imaging data. The viewR picks up tasks from the task manager and is configured so that its functionality suits the user and the task at hand.

To interface the viewR,

#.   Open the viewR website from the landing page.
#.   You should now be able to see the viewR home page with the task manager details including the URL and status. Make sure that the task manager status is ``Online``.
#.   Click on ``start first task`` to complete the first Manual_QA task staged in the task manager.
#.   Annotate the image using the checkboxes and/or the comment field.
#.   ``Save and finish`` your work.
#.   Go to task manager page and check that the status of the task is set to ``done``.
#.   Go to study governer page and check that the experiment has transitioned into a different state.
