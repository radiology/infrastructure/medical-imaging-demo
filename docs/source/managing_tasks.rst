===============================================
Managing Tasks 
===============================================




As stated earlier, an experiment goes through different states until it arrives at the desired final end.  An experiment can only transition from one state to another after going through the procedures (defined as tasks) attached to the current state. A task manager is a separate tool that automatically keeps track of pending tasks that experiments have to pass through. 

For the time being, there are two template tasks defined i.e. Manual_QA and Manual_QC_tasks. You can see in study governor “transitions” tab which state of an experiment is attached to such callbacks/tasks. Task manager retains tasks until they are completed. 

To check the task queue,

#.   Go to the task manager page from the **landing page**.

#.   Log in using the credentials (demo:demo).

#.   Check if the experiment is staged for a task.

Checkout the `Task manager Documentation <https://idsp-task-manager.readthedocs.io/en/latest/>`_ for more information. 
